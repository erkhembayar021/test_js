const express = require('express')
const app = express()
const port = 8085

app.use(express.static('public'));

app.get('/', function (req , res) {
    res.sendFile(__dirname + '/home.html');
});



app.listen(port, () => console.log(`Example app listening on port ${port}!`))
