(function() {
    const myQuestions = [
      {
        question: "1. Which of the following is correct about Bootstrap Media Query?",
        answers: {
          a: "Media queries have two parts, a device specification and then a size rule.",
          b: "Media Queries in Bootstrap allow you to move, show and hide content based on the viewport size.",
          c: "Both of the above."
      },
      correctAnswer: "c"
    },
      {
        question: "2. Which of the following class can be used to create a responsive table?",
        answers: {
          a: ".table-responsive",
          b: ".responsive",
          c: ".active"
        },
        correctAnswer: "a"
      },
      {
        question: "3. Which of the following class is required to be added to form tag to make it inline?",
        answers: {
          a: ".inline",
          b: ".form-inline",
          c: ".horizontal"
        },
        correctAnswer: "b"
      },
      {
        question: "4. Which of the following bootstrap style of image adds a bit of padding and a gray border?",
        answers: {
          a: ".img-rounded",
          b: ".img-circle",
          c: ".img-thumbnail",
          d: "None of the above."
        },
        correctAnswer: "c"
      },
      {
        question: "5. Which of the following bootstrap styles are used to add a dropdown to a tab?",
        answers: {
          a: ".nav, .nav-tabs, .menu",
          b: ".nav, .nav-tabs, dropdown-.menu",
          c: ".nav, .nav-pills, .dropdown",
          d: ".nav, .nav-pills"
        },
        correctAnswer: "b"
      },
      {
        question: "6. Which of the following bootstrap style is to be used if you want the .navbar fixed to the top of the page?",
        answers: {
          a: ".navbar-top",
          b: ".navbar-fixed",
          c: ".navbar-fixed-top",
          d: "None of the above."
  
        },
        correctAnswer: "c"
      },
      {
        question: "7.  Which of the following is correct about Bootstrap jumbotron?",
        answers: {
          a: "This component can optionally increase the size of headings and add a lot of margin for landing page content.",
          b: "To use the Jumbotron: Create a container <div> with the class of .jumbotron.",
          c: "Both of the above.",
          d: "None of the above."
        },
        correctAnswer: "c"
      },
      {
        question: "8. Using which of the following ways you can add header to a panel?",
        answers: {
          a: "Use .panel-heading class to easily add a heading container to your panel.",
          b: "Use any <h1>-<h6> with a .panel-title class to add a pre-styled heading.",
          c: "Both of the above.",
          d: "None of the above."
        },
        correctAnswer: "c"
      },  {
          question: "9. Which of the following is correct about data-show Data attribute of Modal Plugin?",
          answers: {
            a: "It specifies static for a backdrop, if you don't want the modal to be closed when the user clicks outside of the modal.",
            b: "It closes the modal when escape key is pressed; set to false to disable.",
            c: "It shows the modal when initialized.",
            d: "None of the above."
          },
          correctAnswer: "c"
        },
        {
          question: "10. Which of the following is correct about Popover Plugin?",
          answers: {
            a: "You can toggle the Popover plugin's hidden content via data attributes.",
            b: "You can toggle the Popover plugin's hidden content via javascript.",
            c: "Both of the above."
          },
          correctAnswer: "c"
        }
    ];
  
    function buildQuiz() {
      // html garalt hadgalah
      const output = [];
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // hariultuudinn jagsaaltiig hadgalna
        const answers = [];
  
        for (letter in currentQuestion.answers) {
          // radio button nemeh
          answers.push(
            `<label>
               <input type="radio" name="question${questionNumber}" value="${letter}">
                ${letter} :
                ${currentQuestion.answers[letter]}
             </label>`
          );
        }
  
          // asuult blon hariultiin output
        output.push(
          `<div class="slide">
             <div class="question"> ${currentQuestion.question} </div>
             <div class="answers"> ${answers.join("")} </div>
           </div>`
        );
      });
  
      //html garalt
      quizContainer.innerHTML = output.join("");
    }
  
    function showResults() {
      const answerContainers = quizContainer.querySelectorAll(".answers");
  
      // zuw hariulsan hariult
      let numCorrect = 0;
  
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // songoson hariultiig haina
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;
  
        // hariult zow baih bol
        if (userAnswer === currentQuestion.correctAnswer) {
          // zuw hariulsan toog nemne
          numCorrect++;
        }
      }
    );
  
      // zuw hariulsan toog haruulna
      resultsContainer.innerHTML = `Зөв хариулсан : ${numCorrect} Нийт асуулт : ${myQuestions.length}`;
  
    }
    function showScore(){
  
    }
    function showSlide(n) {
      slides[currentSlide].classList.remove("active-slide");
      slides[n].classList.add("active-slide");
      currentSlide = n;
  
      if (currentSlide === 0) {
        previousButton.style.display = "none";
        backButton.style.display = "None"
      } else {
        previousButton.style.display = "inline-block";
        backButton.style.display = "None"
      }
  
      if (currentSlide === slides.length - 1) {
        nextButton.style.display = "none";
        submitButton.style.display = "inline-block";
        backButton.style.display = "inline-block"
      } else {
        nextButton.style.display = "inline-block";
        submitButton.style.display = "none";
        backButton.style.display = "none"
      }
    }
  
  
    function showNextSlide() {
      showSlide(currentSlide + 1);
    }
  
    function showPreviousSlide() {
      showSlide(currentSlide - 1);
    }
  
    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");
  
    // delgets dr asuultuudiig build hiine
    buildQuiz();
  
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    const backButton = document.getElementById("back");
    let currentSlide = 0;
  
    showSlide(0);
  
    // submit bolon vr dung haruulna
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);
  })();
  