const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/quiz');

mongoose.connection.once('open',function(){
    console.log('Connected !');
}).on('error',function(error){
    console.log('Error connection :' ,error);
})