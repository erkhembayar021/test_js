(function() {
    const myQuestions = [
      {
        question: "1. Which of the following is a component of CSS style rule?",
        answers: {
          a: "Selector",
          b: "Property",
          c: "Value",
          d: "All of the above."
      },
      correctAnswer: "d"
    },
      {
        question: "2. Which of the following is a way to associate styles with your HTML document?",
        answers: {
          a: "External CSS - The Element",
          b: "Imported CSS - @import Rule",
          c: "Both of the above.",
          d: "None of the above."
        },
        correctAnswer: "c"
      },
      {
        question: "3. Which of the following defines a measurement in millimeters?",
        answers: {
          a: " in",
          b: "mm",
          c: "pc"
        },
        correctAnswer: "b"
      },
      {
        question: "4. Which of the following property is used to control the position of an image in the background?",
        answers: {
          a: "background-color",
          b: " background-image",
          c: "background-repeat",
          d: "background-position"
        },
        correctAnswer: "d"
      },
      {
        question: "5. Which of the following property is used to add or subtract space between the words of a sentence?",
        answers: {
          a: "text-indent",
          b: "text-align",
          c: "text-decoration",
          d: "text-transform"
        },
        correctAnswer: "a"
      },
      {
        question: "6. Which of the following property of a anchor element signifies unvisited hyperlinks?",
        answers: {
          a: ":link",
          b: ":visited",
          c: ":hover",
          d: ":active"
  
        },
        correctAnswer: "a"
      },
      {
        question: "7. Which of the following property of a anchor element signifies an element on which the user is currently clicking?",
        answers: {
          a: ":link",
          b: ":visited",
          c: ":hover",
          d: ":active"
        },
        correctAnswer: "d"
      },
      {
        question: "8. Which of the following property changes the color of left border?",
        answers: {
          a: ":border-top-color",
          b: ":border-left-color",
          c: ":border-right-color",
          d: ":border-bottom-color"
        },
        correctAnswer: "b"
      },  {
          question: "9. Which of the following property specifies an image for the marker rather than a bullet point or number?",
          answers: {
            a: "list-style-type",
            b: "list-style-position",
            c: "list-style-image",
            d: "list-style"
          },
          correctAnswer: "c"
        },
        {
          question: "10. Which of the following value of cursor shows it as the 'I' bar?",
          answers: {
            a: "crosshair",
            b: "default",
            c: "move"
          },
          correctAnswer: "c"
        }
    ];
  
    function buildQuiz() {
      // html garalt hadgalah
      const output = [];
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // hariultuudinn jagsaaltiig hadgalna
        const answers = [];
  
        for (letter in currentQuestion.answers) {
          // radio button nemeh
          answers.push(
            `<label>
               <input type="radio" name="question${questionNumber}" value="${letter}">
                ${letter} :
                ${currentQuestion.answers[letter]}
             </label>`
          );
        }
  
          // asuult blon hariultiin output
        output.push(
          `<div class="slide">
             <div class="question"> ${currentQuestion.question} </div>
             <div class="answers"> ${answers.join("")} </div>
           </div>`
        );
      });
  
      //html garalt
      quizContainer.innerHTML = output.join("");
    }
  
    function showResults() {
      const answerContainers = quizContainer.querySelectorAll(".answers");
  
      // zuw hariulsan hariult
      let numCorrect = 0;
  
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // songoson hariultiig haina
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;
  
        // hariult zow baih bol
        if (userAnswer === currentQuestion.correctAnswer) {
          // zuw hariulsan toog nemne
          numCorrect++;
        }
      }
    );
  
      // zuw hariulsan toog haruulna
      resultsContainer.innerHTML = `Зөв хариулсан : ${numCorrect} Нийт асуулт : ${myQuestions.length}`;
  
    }
    function showScore(){
  
    }
    function showSlide(n) {
      slides[currentSlide].classList.remove("active-slide");
      slides[n].classList.add("active-slide");
      currentSlide = n;
  
      if (currentSlide === 0) {
        previousButton.style.display = "none";
        backButton.style.display = "None"
      } else {
        previousButton.style.display = "inline-block";
        backButton.style.display = "None"
      }
  
      if (currentSlide === slides.length - 1) {
        nextButton.style.display = "none";
        submitButton.style.display = "inline-block";
        backButton.style.display = "inline-block"
      } else {
        nextButton.style.display = "inline-block";
        submitButton.style.display = "none";
        backButton.style.display = "none"
      }
    }
  
  
    function showNextSlide() {
      showSlide(currentSlide + 1);
    }
  
    function showPreviousSlide() {
      showSlide(currentSlide - 1);
    }
  
    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");
  
    // delgets dr asuultuudiig build hiine
    buildQuiz();
  
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    const backButton = document.getElementById("back");
    let currentSlide = 0;
  
    showSlide(0);
  
    // submit bolon vr dung haruulna
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);
  })();
  