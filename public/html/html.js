(function() {
  const myQuestions = [
    {
      question: "1. Which of the following is true about HTML 5?",
      answers: {
        a: "HTML5 is the next major revision of the HTML standard superseding HTML 4.01, XHTML 1.0, and XHTML 1.1.",
        b: "HTML5 is a standard for structuring and presenting content on the World Wide Web.",
        c: "All of the above."
    },
    correctAnswer: "c"
  },
    {
      question: "2. Which of the following tag represents the footer of a section in HTML5?",
      answers: {
        a: "footer",
        b: "nav",
        c: "section"
      },
      correctAnswer: "a"
    },
    {
      question: "3. Can you use MathML tags directly in HTML5 without any plugin?",
      answers: {
        a: "true",
        b: "false",
      },
      correctAnswer: "a"
    },
    {
      question: "4. Which value of Socket.readyState atribute of WebSocket indicates that the connection has been closed or could not be opened?",
      answers: {
        a: "0",
        b: "1",
        c: "2",
        d: "3"
      },
      correctAnswer: "d"
    },
    {
      question: "5. Which of the following attribute triggers an abort event?",
      answers: {
        a: "offline",
        b: "onabort",
        c: "abort",
        d: "onbeforeonload"
      },
      correctAnswer: "b"
    },
    {
      question: "6. Which of the following attribute triggers event before the document is printed?",
      answers: {
        a: "onbeforeprint",
        b: "onafterprint",
        c: "onprint",
        d: "beforeprint"

      },
      correctAnswer: "a"
    },
    {
      question: "7. Which of the following attribute triggers event when an element leaves a valid drop target?",
      answers: {
        a: "ondrag",
        b: "ondragleave",
        c: "ondragover",
        d: "ondragstart"
      },
      correctAnswer: "b"
    },
    {
      question: "8. Which of the following attribute triggers event when the browser starts to load the media data?",
      answers: {
        a: "onloadedmetadata",
        b: "onloadstart",
        c: "onmessage",
        d: "onoffline"
      },
      correctAnswer: "c"
    },  {
        question: "9. What does HTML stand for?",
        answers: {
          a: "Hyper Text Markup Language",
          b: "Hotmail",
          c: "Both of the above.",
          d: "None of the above."
        },
        correctAnswer: "a"
      },
      {
        question: "10. How many tags are in a regular element?",
        answers: {
          a: "2",
          b: "1",
        },
        correctAnswer: "a"
      }
  ];

  function buildQuiz() {
    // html garalt hadgalah
    const output = [];
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // hariultuudinn jagsaaltiig hadgalna
      const answers = [];

      for (letter in currentQuestion.answers) {
        // radio button nemeh
        answers.push(
          `<label>
             <input type="radio" name="question${questionNumber}" value="${letter}">
              ${letter} :
              ${currentQuestion.answers[letter]}
           </label>`
        );
      }

        // asuult blon hariultiin output
      output.push(
        `<div class="slide">
           <div class="question"> ${currentQuestion.question} </div>
           <div class="answers"> ${answers.join("")} </div>
         </div>`
      );
    });

    //html garalt
    quizContainer.innerHTML = output.join("");
  }

  function showResults() {
    const answerContainers = quizContainer.querySelectorAll(".answers");

    // zuw hariulsan hariult
    let numCorrect = 0;

    myQuestions.forEach((currentQuestion, questionNumber) => {
      // songoson hariultiig haina
      const answerContainer = answerContainers[questionNumber];
      const selector = `input[name=question${questionNumber}]:checked`;
      const userAnswer = (answerContainer.querySelector(selector) || {}).value;

      // hariult zow baih bol
      if (userAnswer === currentQuestion.correctAnswer) {
        // zuw hariulsan toog nemne
        numCorrect++;
      }
    }
  );

    // zuw hariulsan toog haruulna
    resultsContainer.innerHTML = `Зөв хариулсан : ${numCorrect} Нийт асуулт : ${myQuestions.length}`;

  }
  function showScore(){

  }
  function showSlide(n) {
    slides[currentSlide].classList.remove("active-slide");
    slides[n].classList.add("active-slide");
    currentSlide = n;

    if (currentSlide === 0) {
      previousButton.style.display = "none";
      backButton.style.display = "None"
    } else {
      previousButton.style.display = "inline-block";
      backButton.style.display = "None"
    }

    if (currentSlide === slides.length - 1) {
      nextButton.style.display = "none";
      submitButton.style.display = "inline-block";
      backButton.style.display = "inline-block"
    } else {
      nextButton.style.display = "inline-block";
      submitButton.style.display = "none";
      backButton.style.display = "none"
    }
  }


  function showNextSlide() {
    showSlide(currentSlide + 1);
  }

  function showPreviousSlide() {
    showSlide(currentSlide - 1);
  }

  const quizContainer = document.getElementById("quiz");
  const resultsContainer = document.getElementById("results");
  const submitButton = document.getElementById("submit");

  // delgets dr asuultuudiig build hiine
  buildQuiz();

  const previousButton = document.getElementById("previous");
  const nextButton = document.getElementById("next");
  const slides = document.querySelectorAll(".slide");
  const backButton = document.getElementById("back");
  let currentSlide = 0;

  showSlide(0);

  // submit bolon vr dung haruulna
  submitButton.addEventListener("click", showResults);
  previousButton.addEventListener("click", showPreviousSlide);
  nextButton.addEventListener("click", showNextSlide);
})();
