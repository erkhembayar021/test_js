(function() {
    const myQuestions = [
      {
        question: "1. Which of the following is true about variable naming conventions in JavaScript?",
        answers: {
          a: "You should not use any of the JavaScript reserved keyword as variable name.",
          b: "JavaScript variable names should not start with a numeral (0-9).",
          c: "Both of the above."
      },
      correctAnswer: "c"
    },
      {
        question: "2. Which of the following is the correct syntax to create a cookie using JavaScript?",
        answers: {
          a: "document.cookie = 'key1 = value1; key2 = value2; expires = date';",
          b: "browser.cookie = 'key1 = value1; key2 = value2; expires = date';",
          c: "window.cookie = 'key1 = value1; key2 = value2; expires = date';"
        },
        correctAnswer: "a"
      },
      {
        question: "3. Which built-in method returns the character at the specified index?",
        answers: {
          a: "characterAt()",
          b: "getCharAt()",
          c: "charAt()",
        },
        correctAnswer: "c"
      },
      {
        question: "4. Which built-in method returns the calling string value converted to upper case?",
        answers: {
          a: "toUpperCase()",
          b: "toUpper()",
          c: "changeCase(case)",
          d: "None of the above."
        },
        correctAnswer: "a"
      },
      {
        question: "5. Which of the following function of String object returns a number indicating the Unicode value of the character at the given index?",
        answers: {
          a: "charAt()",
          b: "charCodeAt()",
          c: "concat()",
          d: "indexOf()"
        },
        correctAnswer: "b"
      },
      {
        question: "6. Which of the following function of String object extracts a section of a string and returns a new string?",
        answers: {
          a: "slice()",
          b: "split()",
          c: "replace()",
          d: "search()"
  
        },
        correctAnswer: "a"
      },
      {
        question: "7. Which of the following function of String object creates a string to be displayed in a big font as if it were in a <big> tag?",
        answers: {
          a: "anchor()",
          b: "big()",
          c: "blink()",
          d: "italics()"
        },
        correctAnswer: "b"
      },
      {
        question: "8. Which of the following function of String object creates an HTML hypertext link that requests another URL?",
        answers: {
          a: "link()",
          b: "sub()",
          c: "onmessage",
          d: "onoffline"
        },
        correctAnswer: "a"
      },  {
          question: "9. Which of the following function of Array object adds one or more elements to the end of an array and returns the new length of the array?",
          answers: {
            a: "pop()",
            b: "push()",
            c: "Both of the above.",
            d: "None of the above."
          },
          correctAnswer: "b"
        },
        {
          question: "10. Which of the following function of Array object returns a string representing the array and its elements?",
          answers: {
            a: "toSource()",
            b: "sort()",
            c: "toString()",
          },
          correctAnswer: "c"
        }
    ];
  
    function buildQuiz() {
      // html garalt hadgalah
      const output = [];
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // hariultuudinn jagsaaltiig hadgalna
        const answers = [];
  
        for (letter in currentQuestion.answers) {
          // radio button nemeh
          answers.push(
            `<label>
               <input type="radio" name="question${questionNumber}" value="${letter}">
                ${letter} :
                ${currentQuestion.answers[letter]}
             </label>`
          );
        }
  
          // asuult blon hariultiin output
        output.push(
          `<div class="slide">
             <div class="question"> ${currentQuestion.question} </div>
             <div class="answers"> ${answers.join("")} </div>
           </div>`
        );
      });
  
      //html garalt
      quizContainer.innerHTML = output.join("");
    }
  
    function showResults() {
      const answerContainers = quizContainer.querySelectorAll(".answers");
  
      // zuw hariulsan hariult
      let numCorrect = 0;
  
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // songoson hariultiig haina
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;
  
        // hariult zow baih bol
        if (userAnswer === currentQuestion.correctAnswer) {
          // zuw hariulsan toog nemne
          numCorrect++;
        }
      }
    );
  
      // zuw hariulsan toog haruulna
      resultsContainer.innerHTML = `Зөв хариулсан : ${numCorrect} Нийт асуулт : ${myQuestions.length}`;
  
    }
    function showScore(){
  
    }
    function showSlide(n) {
      slides[currentSlide].classList.remove("active-slide");
      slides[n].classList.add("active-slide");
      currentSlide = n;
  
      if (currentSlide === 0) {
        previousButton.style.display = "none";
        backButton.style.display = "None"
      } else {
        previousButton.style.display = "inline-block";
        backButton.style.display = "None"
      }
  
      if (currentSlide === slides.length - 1) {
        nextButton.style.display = "none";
        submitButton.style.display = "inline-block";
        backButton.style.display = "inline-block"
      } else {
        nextButton.style.display = "inline-block";
        submitButton.style.display = "none";
        backButton.style.display = "none"
      }
    }
  
  
    function showNextSlide() {
      showSlide(currentSlide + 1);
    }
  
    function showPreviousSlide() {
      showSlide(currentSlide - 1);
    }
  
    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");
  
    // delgets dr asuultuudiig build hiine
    buildQuiz();
  
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    const backButton = document.getElementById("back");
    let currentSlide = 0;
  
    showSlide(0);
  
    // submit bolon vr dung haruulna
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);
  })();
  