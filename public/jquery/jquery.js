(function() {
    const myQuestions = [
      {
        question: "1. How can you get the type of arguments passed to a function?",
        answers: {
          a: "using typeof operator",
          b: "using getType function",
          c: "All of the above."
      },
      correctAnswer: "a"
    },
      {
        question: "2. Which built-in method reverses the order of the elements of an array?",
        answers: {
          a: "changeOrder(order)",
          b: "reverse()",
          c: "section"
        },
        correctAnswer: "b"
      },
      {
        question: "3. Which of the following jQuery selector selects elements with the given element tag-name?",
        answers: {
          a: "$('tag-name')",
          b: "$('#tag-name')",
          b: "$('.tag-name')"
        },
        correctAnswer: "a"
      },
      {
        question: "4. Which of the following jQuery method adds the specified class if it is not present, remove the specified class if it is present?",
        answers: {
          a: "toggleStyleClass( class )",
          b: "toggleClass( class )",
          c: "toggleCSSClass( class )",
          d: "None of the above."
        },
        correctAnswer: "b"
      },
      {
        question: "5. Which of the following jQuery method selects a subset of the matched elements?",
        answers: {
          a: "subset( selector )",
          b: "getSubset( selector )",
          c: "slice(selector)",
          d: "onbeforeonload"
        },
        correctAnswer: "c"
      },
      {
        question: "6. Which of the following jQuery method gets the direct parent of an element?",
        answers: {
          a: "offsetParent( selector )",
          b: "offset( selector)",
          c: "parent( selector )",
          d: "beforeprint"
  
        },
        correctAnswer: "c"
      },
      {
        question: "7. Which of the following jQuery method gets the height property of an element?",
        answers: {
          a: "getCSSHeight( )",
          b: "getHeight( )",
          c: "height( )",
          d: "ondragstart"
        },
        correctAnswer: "c"
      },
      {
        question: "8. Which of the following jQuery method prevents the browser from executing the default action?",
        answers: {
          a: "preventDefault( )",
          b: "stopImmediatePropagation( )",
          c: "stopPropagation( )",
          d: "onoffline"
        },
        correctAnswer: "a"
      },  {
          question: "9. Which of the following jQuery method binds a handler to one or more events (like click) for an element?",
          answers: {
            a: "bind( type, [data], fn )",
            b: "load(type, [data], fn )",
            c: "Both of the above.",
            d: "None of the above."
          },
          correctAnswer: "a"
        },
        {
          question: "10. Which of the following jQuery method setups global settings for AJAX requests?",
          answers: {
            a: "jQuery.ajax( options )",
            b: "jQuery.ajaxSetup( options )",
          },
          correctAnswer: "b"
        }
    ];
  
    function buildQuiz() {
      // html garalt hadgalah
      const output = [];
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // hariultuudinn jagsaaltiig hadgalna
        const answers = [];
  
        for (letter in currentQuestion.answers) {
          // radio button nemeh
          answers.push(
            `<label>
               <input type="radio" name="question${questionNumber}" value="${letter}">
                ${letter} :
                ${currentQuestion.answers[letter]}
             </label>`
          );
        }
  
          // asuult blon hariultiin output
        output.push(
          `<div class="slide">
             <div class="question"> ${currentQuestion.question} </div>
             <div class="answers"> ${answers.join("")} </div>
           </div>`
        );
      });
  
      //html garalt
      quizContainer.innerHTML = output.join("");
    }
  
    function showResults() {
      const answerContainers = quizContainer.querySelectorAll(".answers");
  
      // zuw hariulsan hariult
      let numCorrect = 0;
  
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // songoson hariultiig haina
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;
  
        // hariult zow baih bol
        if (userAnswer === currentQuestion.correctAnswer) {
          // zuw hariulsan toog nemne
          numCorrect++;
        }
      }
    );
  
      // zuw hariulsan toog haruulna
      resultsContainer.innerHTML = `Зөв хариулсан : ${numCorrect} Нийт асуулт : ${myQuestions.length}`;
  
    }
    function showScore(){
  
    }
    function showSlide(n) {
      slides[currentSlide].classList.remove("active-slide");
      slides[n].classList.add("active-slide");
      currentSlide = n;
  
      if (currentSlide === 0) {
        previousButton.style.display = "none";
        backButton.style.display = "None"
      } else {
        previousButton.style.display = "inline-block";
        backButton.style.display = "None"
      }
  
      if (currentSlide === slides.length - 1) {
        nextButton.style.display = "none";
        submitButton.style.display = "inline-block";
        backButton.style.display = "inline-block"
      } else {
        nextButton.style.display = "inline-block";
        submitButton.style.display = "none";
        backButton.style.display = "none"
      }
    }
  
  
    function showNextSlide() {
      showSlide(currentSlide + 1);
    }
  
    function showPreviousSlide() {
      showSlide(currentSlide - 1);
    }
  
    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");
  
    // delgets dr asuultuudiig build hiine
    buildQuiz();
  
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    const backButton = document.getElementById("back");
    let currentSlide = 0;
  
    showSlide(0);
  
    // submit bolon vr dung haruulna
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);
  })();
  