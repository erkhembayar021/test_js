const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const questionSchema = new Schema({
    id : Number,
    questions : String,
    tag : String
})

const question = mongoose.model('question', questionSchema);

module.exports = question;