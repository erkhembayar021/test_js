(function() {
    const myQuestions = [
      {
        question: "1. MongoDB does not support which type of sorting?",
        answers: {
          a: "Collection",
          b: "Heap",
          c: "Collation",
          d: "All of the above."
      },
      correctAnswer: "c"
    },
      {
        question: "2. Mongo DB is generally used at the _________________.",
        answers: {
          a: "Middleware",
          b: "Frontend",
          c: "Backend",
          d: "None of the above."
        },
        correctAnswer: "c"
      },
      {
        question: "3. If protection in data center fails, we need to keep at least how many members in alternate data center?",
        answers: {
          a: "3",
          b: "2",
          c: "1"
        },
        correctAnswer: "c"
      },
      {
        question: "4. Which of the following statements is true?",
        answers: {
          a: "None of above.",
          b: "MongoDB cannot be used as a file system.",
          c: "MongoDB can run over single servers only.",
          d: "Embedded documents and arrays reduce need for joins."
        },
        correctAnswer: "d"
      },
      {
        question: "5. Fixed size collation in MongoDB is called ________.",
        answers: {
          a: "Capped",
          b: "Secondary",
          c: "None of above",
          d: "Primary"
        },
        correctAnswer: "a"
      },
      {
        question: "6. What should be the priority of member to prevent them from becoming primary?",
        answers: {
          a: "3",
          b: "2",
          c: "1",
          d: "0"
  
        },
        correctAnswer: "d"
      },
      {
        question: "7.We search for fields, queries etc. in MongoDB using _________.",
        answers: {
          a: "C++",
          b: "C",
          c: "JS",
          d: "All of above"
        },
        correctAnswer: "c"
      },
      {
        question: "8. MongoDB scales horizontally using which terminology for load balancing?",
        answers: {
          a: "Partitioning",
          b: "Replication",
          c: "Sharding",
          d: "All of abover"
        },
        correctAnswer: "c"
      },  {
          question: "9. Which sets also allow the routing of read operations to specific machines?",
          answers: {
            a: "Read",
            b: "Tag",
            c: "None of above",
            d: "Field"
          },
          correctAnswer: "b"
        },
        {
          question: "Which of following statement is correct?",
          answers: {
            a: "Fault tolerance is an effect of replica set size, but the relationship is not direct",
            b: "Removing a member to the replica set does not always increase the fault tolerance",
            c: "None of above"
          },
          correctAnswer: "a"
        }
    ];
  
    function buildQuiz() {
      // html garalt hadgalah
      const output = [];
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // hariultuudinn jagsaaltiig hadgalna
        const answers = [];
  
        for (letter in currentQuestion.answers) {
          // radio button nemeh
          answers.push(
            `<label>
               <input type="radio" name="question${questionNumber}" value="${letter}">
                ${letter} :
                ${currentQuestion.answers[letter]}
             </label>`
          );
        }
  
          // asuult blon hariultiin output
        output.push(
          `<div class="slide">
             <div class="question"> ${currentQuestion.question} </div>
             <div class="answers"> ${answers.join("")} </div>
           </div>`
        );
      });
  
      //html garalt
      quizContainer.innerHTML = output.join("");
    }
  
    function showResults() {
      const answerContainers = quizContainer.querySelectorAll(".answers");
  
      // zuw hariulsan hariult
      let numCorrect = 0;
  
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // songoson hariultiig haina
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;
  
        // hariult zow baih bol
        if (userAnswer === currentQuestion.correctAnswer) {
          // zuw hariulsan toog nemne
          numCorrect++;
        }
      }
    );
  
      // zuw hariulsan toog haruulna
      resultsContainer.innerHTML = `Зөв хариулсан : ${numCorrect} Нийт асуулт : ${myQuestions.length}`;
  
    }
    function showScore(){
  
    }
    function showSlide(n) {
      slides[currentSlide].classList.remove("active-slide");
      slides[n].classList.add("active-slide");
      currentSlide = n;
  
      if (currentSlide === 0) {
        previousButton.style.display = "none";
        backButton.style.display = "None"
      } else {
        previousButton.style.display = "inline-block";
        backButton.style.display = "None"
      }
  
      if (currentSlide === slides.length - 1) {
        nextButton.style.display = "none";
        submitButton.style.display = "inline-block";
        backButton.style.display = "inline-block"
      } else {
        nextButton.style.display = "inline-block";
        submitButton.style.display = "none";
        backButton.style.display = "none"
      }
    }
  
  
    function showNextSlide() {
      showSlide(currentSlide + 1);
    }
  
    function showPreviousSlide() {
      showSlide(currentSlide - 1);
    }
  
    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");
  
    // delgets dr asuultuudiig build hiine
    buildQuiz();
  
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    const backButton = document.getElementById("back");
    let currentSlide = 0;
  
    showSlide(0);
  
    // submit bolon vr dung haruulna
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);
  })();
  