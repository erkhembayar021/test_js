(function() {
  const myQuestions = [
    {
      question: "In which of the following areas, Node.js is not advised to be used?",
      answers: {
        a: "Single Page Applications",
        b: " JSON APIs based Applications",
        c: "CPU intensive applications"
      },
      correctAnswer: "c"
    },
    {
      question: "What is Callback?",
      answers: {
        a: " Callback is an asynchronous equivalent for a function.",
        b: " Callback is a technique in which a method call back the caller method.",
        c: " None of the above."
      },
      correctAnswer: "a"
    },
    {
      question: " Which of the following is true about EventEmitter.on property?",
      answers: {
        a: "on property is used to fire event.",
        b: " on property is used to bind a function with the event.",
        c: "on property is used to locate an event handler.",
        d: "None of the above."
      },
      correctAnswer: "b"
    },
    {
      question: " Which of the following is true about Chaining streams?",
      answers: {
        a: "Chanining is a mechanism to connect output of one stream to another stream and create a chain of multiple stream operations.",
        b: "Chanining is normally used with piping operations.",
        c: "Both of the above.",
        d: "None of the above."
      },
      correctAnswer: "c"
    },
    {
      question: "Which of the following is true about __filename global object?",
      answers: {
        a: "The __filename represents the filename of the code being executed.",
        b: "The __filename represents the resolved absolute path of code file.",
        c: "Both of the above.",
        d: "None of the above."
      },
      correctAnswer: "c"
    },
    {
      question: " Is process a global object?",
      answers: {
        a: "true",
        b: "false",

      },
      correctAnswer: "a"
    },
    {
      question: "Which of the following API creates a server?",
      answers: {
        a: "net.createServer([options][, connectionListener])",
        b: "net.connect(options[, connectionListener])",
        c: "net.createConnection(port[, host][, connectListener])",
        d: "None of the above."
      },
      correctAnswer: "a"
    },
    {
      question: "Which of the following module is required for exception handling in Node?",
      answers: {
        a: "web module",
        b: "net module",
        c: "domain module",
        d: "error module"
      },
      correctAnswer: "c"
    },  {
        question: "Which of the following is true about fork methd of child_process module.",
        answers: {
          a: "The fork() method method is a special case of the spawn() to create Node processes.",
          b: "The fork method returns object with a built-in communication channel in addition to having all the methods in a normal ChildProcess instance.",
          c: "Both of the above.",
          d: "None of the above."
        },
        correctAnswer: "c"
      },
      {
        question: "Child processes always have three streams child.stdin, child.stdout, and child.stderr which may be shared with the stdio streams of the parent process.",
        answers: {
          a: "true",
          b: "false",
        },
        correctAnswer: "a"
      }
  ];

  function buildQuiz() {
    // html garalt hadgalah
    const output = [];
    myQuestions.forEach((currentQuestion, questionNumber) => {
      // hariultuudinn jagsaaltiig hadgalna
      const answers = [];

      for (letter in currentQuestion.answers) {
        // radio button nemeh
        answers.push(
          `<label>
             <input type="radio" name="question${questionNumber}" value="${letter}">
              ${letter} :
              ${currentQuestion.answers[letter]}
           </label>`
        );
      }

        // asuult blon hariultiin output
      output.push(
        `<div class="slide">
           <div class="question"> ${currentQuestion.question} </div>
           <div class="answers"> ${answers.join("")} </div>
         </div>`
      );
    });

    //html garalt
    quizContainer.innerHTML = output.join("");
  }

  function showResults() {
    const answerContainers = quizContainer.querySelectorAll(".answers");

    // zuw hariulsan hariult
    let numCorrect = 0;

    myQuestions.forEach((currentQuestion, questionNumber) => {
      // songoson hariultiig haina
      const answerContainer = answerContainers[questionNumber];
      const selector = `input[name=question${questionNumber}]:checked`;
      const userAnswer = (answerContainer.querySelector(selector) || {}).value;

      // hariult zow baih bol
      if (userAnswer === currentQuestion.correctAnswer) {
        // zuw hariulsan toog nemne
        numCorrect++;
      }
    }
  );

    // zuw hariulsan toog haruulna
    resultsContainer.innerHTML = `Зөв хариулсан : ${numCorrect} Нийт асуулт : ${myQuestions.length}`;

  }
  function showScore(){

  }
  function showSlide(n) {
    slides[currentSlide].classList.remove("active-slide");
    slides[n].classList.add("active-slide");
    currentSlide = n;

    if (currentSlide === 0) {
      previousButton.style.display = "none";
      backButton.style.display = "None"
    } else {
      previousButton.style.display = "inline-block";
      backButton.style.display = "None"
    }

    if (currentSlide === slides.length - 1) {
      nextButton.style.display = "none";
      submitButton.style.display = "inline-block";
      backButton.style.display = "inline-block"
    } else {
      nextButton.style.display = "inline-block";
      submitButton.style.display = "none";
      backButton.style.display = "none"
    }
  }


  function showNextSlide() {
    showSlide(currentSlide + 1);
  }

  function showPreviousSlide() {
    showSlide(currentSlide - 1);
  }

  const quizContainer = document.getElementById("quiz");
  const resultsContainer = document.getElementById("results");
  const submitButton = document.getElementById("submit");

  // delgets dr asuultuudiig build hiine
  buildQuiz();

  const previousButton = document.getElementById("previous");
  const nextButton = document.getElementById("next");
  const slides = document.querySelectorAll(".slide");
  const backButton = document.getElementById("back");
  let currentSlide = 0;

  showSlide(0);

  // submit bolon vr dung haruulna
  submitButton.addEventListener("click", showResults);
  previousButton.addEventListener("click", showPreviousSlide);
  nextButton.addEventListener("click", showNextSlide);
})();
