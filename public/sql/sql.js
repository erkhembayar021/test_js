(function() {
    const myQuestions = [
      {
        question: "1. Which command is used to remove table along with the data?",
        answers: {
          a: "TRUNCATE",
          b: "REMOVE",
          c: "DELETE",
          d: "DROP"
      },
      correctAnswer: "d"
    },
      {
        question: "2. Which function is used to get current time?",
        answers: {
          a: "CURRENTTIME()",
          b: "DATETIME()",
          c: "CURRRENT_TIME()",
          d: "None of the above."
        },
        correctAnswer: "c"
      },
      {
        question: "3. Which of the following clause is used to delete entire MySQL database?",
        answers: {
          a: "mysql_drop",
          b: "mysql_drop_db",
          c: "DROP DATABASE"
        },
        correctAnswer: "b"
      },
      {
        question: "4. For accessing data from the MySQL database you will need _______________.",
        answers: {
          a: "Database client",
          b: "FTP",
          c: "Java",
          d: "Telnet"
        },
        correctAnswer: "a"
      },
      {
        question: "5. Which of the following clause is used to limit the number of rows via a query?",
        answers: {
          a: "LIMIT",
          b: "AND",
          c: "HAVING",
          d: "FROM"
        },
        correctAnswer: "a"
      },
      {
        question: "6. We use USE keyword to select a ___________.",
        answers: {
          a: "COLUMN",
          b: "TABLE",
          c: "DATABASE",
          d: "ROW"
  
        },
        correctAnswer: "c"
      },
      {
        question: "7. A view is a ___________ table.",
        answers: {
          a: "Dynamic",
          b: "Virtual",
          c: "Static",
          d: "Real"
        },
        correctAnswer: "b "
      },
      {
        question: "8. MySQL is ___________________.",
        answers: {
          a: "RDBMS",
          b: "NoSQL",
          c: "DBMS",
          d: "All of above"
        },
        correctAnswer: "a"
      },  {
          question: "9. How much space is occupied by DATETIME datatype?",
          answers: {
            a: "1 byte",
            b: "8 bytes",
            c: "4 bytes",
            d: "2 bytes"
          },
          correctAnswer: "b"
        },
        {
          question: "10. Can a BLOB have a default value?",
          answers: {
            a: "Yes",
            b: "No",
            c: "None of above."
          },
          correctAnswer: "b"
        }
    ];
  
    function buildQuiz() {
      // html garalt hadgalah
      const output = [];
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // hariultuudinn jagsaaltiig hadgalna
        const answers = [];
  
        for (letter in currentQuestion.answers) {
          // radio button nemeh
          answers.push(
            `<label>
               <input type="radio" name="question${questionNumber}" value="${letter}">
                ${letter} :
                ${currentQuestion.answers[letter]}
             </label>`
          );
        }
  
          // asuult blon hariultiin output
        output.push(
          `<div class="slide">
             <div class="question"> ${currentQuestion.question} </div>
             <div class="answers"> ${answers.join("")} </div>
           </div>`
        );
      });
  
      //html garalt
      quizContainer.innerHTML = output.join("");
    }
  
    function showResults() {
      const answerContainers = quizContainer.querySelectorAll(".answers");
  
      // zuw hariulsan hariult
      let numCorrect = 0;
  
      myQuestions.forEach((currentQuestion, questionNumber) => {
        // songoson hariultiig haina
        const answerContainer = answerContainers[questionNumber];
        const selector = `input[name=question${questionNumber}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;
  
        // hariult zow baih bol
        if (userAnswer === currentQuestion.correctAnswer) {
          // zuw hariulsan toog nemne
          numCorrect++;
        }
      }
    );
  
      // zuw hariulsan toog haruulna
      resultsContainer.innerHTML = `Зөв хариулсан : ${numCorrect} Нийт асуулт : ${myQuestions.length}`;
  
    }
    function showScore(){
  
    }
    function showSlide(n) {
      slides[currentSlide].classList.remove("active-slide");
      slides[n].classList.add("active-slide");
      currentSlide = n;
  
      if (currentSlide === 0) {
        previousButton.style.display = "none";
        backButton.style.display = "None"
      } else {
        previousButton.style.display = "inline-block";
        backButton.style.display = "None"
      }
  
      if (currentSlide === slides.length - 1) {
        nextButton.style.display = "none";
        submitButton.style.display = "inline-block";
        backButton.style.display = "inline-block"
      } else {
        nextButton.style.display = "inline-block";
        submitButton.style.display = "none";
        backButton.style.display = "none"
      }
    }
  
  
    function showNextSlide() {
      showSlide(currentSlide + 1);
    }
  
    function showPreviousSlide() {
      showSlide(currentSlide - 1);
    }
  
    const quizContainer = document.getElementById("quiz");
    const resultsContainer = document.getElementById("results");
    const submitButton = document.getElementById("submit");
  
    // delgets dr asuultuudiig build hiine
    buildQuiz();
  
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    const slides = document.querySelectorAll(".slide");
    const backButton = document.getElementById("back");
    let currentSlide = 0;
  
    showSlide(0);
  
    // submit bolon vr dung haruulna
    submitButton.addEventListener("click", showResults);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", showNextSlide);
  })();
  